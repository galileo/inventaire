#!/bin/bash

IFS=$'\n'  

DATES="2021-08|2021-07" # 2021-08|2021-07
TMP="tmp/"
FICHIER="tmp/export.csv"
SERV="ddt"
SERVEX="ddcs" # service à exclure
IP="10.72.11"

# l'export des modèle doit être fait seulement une fois car le fichier modele.txt est modifié manuellement
#cat $FICHIER | grep -i "$SERV"| egrep "$DATES"| cut -d ";" -f 5| sort -u > efface.txt
#cat efface.txt |sed 's/\"$/\";\"\"/g' > modeles.txt
#rm efface.txt

for i in `cat tmp/modeles.txt |egrep ";\"P\""|cut -d ";" -f1`; do grep $i $FICHIER ; done > $TMP/portables.csv

echo "Le nombre de machines pour le service $SERV connecté en $DATES"
cat $TMP/export.csv |egrep -i "$SERV|$IP"|egrep "$DATES"|grep -v "$SERVEX"|wc -l

echo "Le nombre de portable pour le service $SERV connecté en $DATES"
cat $TMP/portables.csv  |egrep -i "$SERV|$IP"|egrep "$DATES"|grep -v "$SERVEX"|wc -l

echo "Nombre de machines de moins de 5 ans"
cat tmp/export.csv | egrep -i "$SERV|$IP"|egrep "$DATES"|grep -v "$SERVEX"| cut -d ";" -f 9 > tmp/annees.txt
for i in $(seq 2017 2021 ) ; do grep $i tmp/annees.txt;done|wc -l


#for i in `cat tmp/utilisateurs.txt`
#do
	#echo
	#echo $i
	#NOMBRE=$(grep $i tmp/export.csv|grep "$DATES"|wc -l)
	
	#case $NOMBRE in
	
	#0)
    #echo "nb session : 0"
    #;;

    #1)
	#echo "nb session : 1"
    #;;

    #*)
    #echo "nb session : 2 ou +"
    #;;
#esac

	
	#if [ $? -eq 0 ]; then
	#	echo "l'utilisateur a ouvert une session au moins une fois au cours de $DATES"
	#else
	#	echo "pas de sessions ouvertes"
	#fi
		
#done

