
# sript export.sh

ce script permet de créer le fichier ocs.csv en passant le CSRF

# inventaire

Analyse d'un fichier d'export OCS pour une recherche part type et par âge 

La première étape est l'export d'un fichier csv à partir d'OCS contenant les colonnes suivantes 

```
1 - Account info: TAG;
2 - Dernier inventaire;
3 - Machine;
4 - RAM (Mo);
5 - Modèle;
6 - Adresse IP;
7 - Date BIOS;
```

## liste des modèles

```
cat export.csv | grep -i "ddt"| egrep "2021-08|2021-07"| cut -d ";" -f 5| sort -u > efface.txt
```

```
cat efface.txt |sed 's/\"$/\";\"\"/g' > modeles.txt
rm efface.txt
```

cette liste doit être complétée à la main 

```
…
"10VGS05N00";""
"20JRS0VW00";""
…
```


devient 

```
…
"10VGS05N00";"F"
"20JRS0VW00";"P"
…
```

F=Fixe , P=Portable , S=Serveur

## liste des fixes

```
for i in `cat modeles.txt |egrep ";\"F\""|cut -d ";" -f1`; do grep $i export.csv ; done > fixe.csv
```

## liste des portables

```
for i in `cat modeles.txt |egrep ";\"P\""|cut -d ";" -f1`; do grep $i export.csv ; done > portable.csv
```

## bios date

liste des ordinateurs DDT dont la dernière connexion date de juillet ou août 2021

```
cat export.csv | grep -i "ddt"| egrep "2021-08|2021-07"| cut -d ";" -f 8 > annees.txt
```

```
"07/19/2017"
"06/19/2020"
"12/04/2018"
"07/19/2017"
"07/19/2017"
````

## moins de 5 ans

nombre d'ordinateur dont le champ «bios date» est de moins de 5 ans

```
for i in $(seq 2017 2021 ) ; do grep $i annees.txt;done|wc 
```
