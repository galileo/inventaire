#!/bin/bash
# Auteur : Nordine VALLAS 
# gestion de la protection CSRF : Pascal Galicier SIDSIC89   25/11/2014 
# contexte : outils d'extraction de données d'un ocs sans API

if test -e cookies.txt
then
	exit 0
fi

# config.sh
# url=https://serveur.dom/sub
# login=id
# mdp=password
source config.sh 

#Bande passante utilisée (Auxerre = 10Mbps = 1280KB/s : on prends le quart : 320K
limite_bande=320k


# cookies et du token : CSRF Pascal Galicier SIDSIC89   25/11/2014
token=$(curl -c cookies.txt -b cookies.txt  $OCSURL/index.php 2>/dev/null|grep "CSRF_0"|sed "s/.*CSRF_0' value='//g"|cut -d "'" -f 1)
echo "1 : $token"
token=$(curl -c cookies.txt -b cookies.txt -d "CSRF_0=$token&LOGIN=$LOGIN&PASSWD=$PASW&Valid_CNX=Envoyer" $OCSURL/index.php 2>/dev/null|grep "CSRF_2" |sed "s/.*CSRF_2' value='//g"|cut -d "'" -f 1)
echo "2 : $token"


# requête principale
#curl -c cookies.txt -b cookies.txt  -d "CSRF_2=$token&select_collist_show_all=default&COL_SEARCH=default&list_show_all_length=10000&SUP_PROF=&MODIF=&SELECT=&OTHER=&ACTIVE=&CONFIRM_CHECK=&OTHER_BIS=&OTHER_TER=&DEL_ALL="   "$OCSURL/ajax.php?function=visu_computers&no_header=true&no_footer=true"  1>ocs.html 2>/dev/null
curl -v -c cookies.txt -b cookies.txt  -d "CSRF_2=$token&select_collist_show_all=macaddr&COL_SEARCH=default&list_show_all_length=10000&SUP_PROF=&MODIF=&SELECT=&OTHER=&ACTIVE=&CONFIRM_CHECK=&OTHER_BIS=&OTHER_TER=&DEL_ALL=" "$OCSURL/ajax.php?function=visu_computers&no_header=true&no_footer=true"  1>ocs.html 2>/dev/null

curl -c cookies.txt -b cookies.txt "$OCSURL/index.php?function=export_csv&no_header=1&tablename=list_show_all&nolimit=true&base=&select_collist_show_all=macaddr"  1>ocs.csv 2>/dev/null

rm cookies.txt
